# Changelog <!-- omit in toc -->

- [1.x.x](#1xx)
  - [1.2.0 - 24-Jan-2021](#120---24-jan-2021)
  - [1.1.0 - 24-Jan-2021](#110---24-jan-2021)
  - [1.0.0 - 24-Jan-2021](#100---24-jan-2021)
- [Pre-release versions](#pre-release-versions)
  - [0.1.0 - 24-Jan-2021](#010---24-jan-2021)

## 1.x.x

### 1.2.0 - 24-Jan-2021

- Replace build-script tests with macro tests
  - Hopefully leads to quicker build times and lower dependency count
    - Removed `serde` / `serde_json` build dependency
    - Moved `anyhow` build dependency to dev dependencies
- Bumped `nom` version to `6.1.0`

### 1.1.0 - 24-Jan-2021

- Derive traits on `UriTemplate`:
  ```rust
  #[derive(Clone, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
  ```

### 1.0.0 - 24-Jan-2021

- Initial "stable" release and publication to [crates.io](https://crates.io/crates/rfc6570-level-2)

## Pre-release versions

### 0.1.0 - 24-Jan-2021

- Initial pre-release version
