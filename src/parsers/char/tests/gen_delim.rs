use anyhow::Context;

test_cases! {
    char::gen_delim: {
        case_at: "@",
        case_colon: ":",
        case_forward_slash: "/",
        case_hash: "#",
        case_question_mark: "?",
        case_square_bracket_left: "[",
        case_square_bracket_right: "]"
    }
}
